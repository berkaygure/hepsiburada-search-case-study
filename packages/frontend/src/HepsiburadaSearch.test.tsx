// import { render } from '@testing-library/react';
import HepsiburadaSearch from 'HepsiburadaSearch';
import { fireEvent, render, screen } from 'test-utils';
import { DEFAULT_SEARCH_PLACEHOLDER } from 'utils/constants';

describe('TEST App', () => {
  it('should render page', () => {
    render(<HepsiburadaSearch />);

    expect(screen.queryByText('Sepetim')).toBeDefined();
    expect(screen.queryByText(DEFAULT_SEARCH_PLACEHOLDER)).toBeDefined();
    expect(screen.queryByText('Siralama')).toBeDefined();
  });
});
