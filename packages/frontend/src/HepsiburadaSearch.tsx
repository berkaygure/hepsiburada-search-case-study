import Header from 'components/Header';
import Layout from 'components/Layout';
import CartContextProvider from 'context/CartContext';
import { SearchProvider } from 'context/SearchContext';
import ProductSearch from 'pages/ProductSearch';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

const HepsiburadaSearch: React.FC = () => {
  return (
    <Router>
      <SearchProvider>
        <CartContextProvider>
          <Switch>
            <Route>
              <Layout className="App">
                <Layout.Header>
                  <Header />
                </Layout.Header>
                <ProductSearch />
              </Layout>
            </Route>
          </Switch>
        </CartContextProvider>
      </SearchProvider>
    </Router>
  );
};

export default HepsiburadaSearch;
