import { FilterItemProps } from 'components/Filter';

export interface ProductFilterQuery {
  q?: string;
  filters?: string;
  order?: string;
  page?: number;
}

export interface ProductAttribute {
  id: string;
  value: string;
  name: string;
  title: string;
}

export interface ProductInterface {
  id: number;
  title: string;
  image: string;
  listPrice: number;
  discount: number;
  discountedPrice: number;
  attributes: ProductAttribute[];
}

export interface FilterResult {
  products: ProductInterface[];
  categoryTitle: string;
  filters: {
    id: string;
    title: string;
    items: FilterItemProps[];
  }[];
  page: number;
  total: number;
  count: number;
}

const filterProductsAPI = async (filter: string): Promise<FilterResult> => {
  const response = await fetch(`${process.env.REACT_APP_BACKEND_HOST}/search${filter}`);
  const productsListJson = await response.json();

  return productsListJson;
};

export { filterProductsAPI };
