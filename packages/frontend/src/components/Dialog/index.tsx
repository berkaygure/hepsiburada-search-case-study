import { ReactNode } from 'react';
import ReactDOM from 'react-dom';
import Modal, { ModalProps } from 'components/Modal';

type DialogProps = ModalProps;

function createDialog(props: DialogProps) {
  const div = document.createElement('div');
  document.body.appendChild(div);

  const currentConfig = {
    visible: true,
    ...props,
  } as DialogProps;

  function destroy() {
    const unmountResult = ReactDOM.unmountComponentAtNode(div);
    if (unmountResult && div.parentNode) {
      div.parentNode.removeChild(div);
    }
  }

  function render() {
    ReactDOM.render(
      <Modal
        {...currentConfig}
        onCancel={() => {
          destroy();
          if (currentConfig.onCancel) {
            currentConfig.onCancel();
          }
        }}
        onOk={() => {
          destroy();
          if (currentConfig.onOk) {
            currentConfig.onOk();
          }
        }}
      >
        {props.children}
      </Modal>,
      div,
    );
  }

  render();

  return destroy;
}

function withConfirm(props: DialogProps): DialogProps {
  return {
    ...props,
  };
}

function withAlert(props: DialogProps): DialogProps {
  return {
    showCancel: false,
    okText: 'Tamam',
    ...props,
  };
}

const Dialog = {
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  confirm(props: DialogProps) {
    return createDialog(withConfirm(props));
  },
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  alert(title: string, message: ReactNode) {
    return createDialog(withAlert({ title, children: message }));
  },
};

export default Dialog;
