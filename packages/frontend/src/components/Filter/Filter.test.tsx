import { fireEvent, render, screen } from '@testing-library/react';
import Filter, { FilterItemProps } from '.';
import style from './filter.module.scss';

const filterItems: FilterItemProps[] = [
  {
    title: 'Blue',
    value: 'blue',
    count: 1,
  },
  {
    title: 'Red',
    value: 'red',
    count: 2,
  },
  {
    title: 'Yellow',
    value: 'yellow',
  },
];

const FILTER_NAME = 'Color';

describe('TEST filter', () => {
  it('should render filters', () => {
    render(<Filter title={FILTER_NAME} items={filterItems} onChange={() => []} />);

    expect(screen.getByTestId('filterTitle')).toHaveTextContent(FILTER_NAME);
    expect(screen.getAllByRole('button')).toHaveLength(filterItems.length);
  });

  it('should make selected active filter', () => {
    render(<Filter title={FILTER_NAME} items={filterItems} values={['red']} onChange={() => []} />);

    expect(screen.getByText('Red (2)')).toHaveClass(style.selected);
  });

  it('should not render when list items empty', () => {
    render(<Filter title={FILTER_NAME} values={[]} onChange={() => []} />);

    expect(screen.queryAllByRole('button')).toHaveLength(0);
  });

  it('should make selected active filters', () => {
    render(
      <Filter
        title={FILTER_NAME}
        items={filterItems}
        values={['red', 'blue']}
        onChange={() => []}
      />,
    );

    expect(screen.getByText('Red (2)')).toHaveClass(style.selected);
    expect(screen.getByText('Blue (1)')).toHaveClass(style.selected);
  });

  it('should add active a filter', () => {
    const onClick = jest.fn();
    render(<Filter title={FILTER_NAME} items={filterItems} values={['blue']} onChange={onClick} />);

    fireEvent.click(screen.getByText('Red (2)'));

    expect(onClick).toHaveBeenCalledWith(['blue', 'red']);
  });

  it('should remove active filter when click', () => {
    const onClick = jest.fn();
    render(<Filter title={FILTER_NAME} items={filterItems} values={['blue']} onChange={onClick} />);

    fireEvent.click(screen.getByText('Blue (1)'));

    expect(onClick).toHaveBeenCalledWith([]);
  });

  it('should NOT render count when it disabled', () => {
    render(
      <Filter
        title={FILTER_NAME}
        items={filterItems}
        showCount={false}
        values={['blue']}
        onChange={() => []}
      />,
    );

    expect(screen.getByText('Red')).toBeDefined();
  });

  it('should default render counts', () => {
    render(
      <Filter title={FILTER_NAME} items={filterItems} values={['blue']} onChange={() => []} />,
    );

    expect(screen.getByText('Red (2)')).toBeDefined();
  });
});
