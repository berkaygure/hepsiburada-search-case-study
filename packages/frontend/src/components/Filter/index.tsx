import cn from 'classnames';
import style from './filter.module.scss';

export interface FilterItemProps {
  title: string;
  value: string;
  count?: number;
}

interface FilterProps {
  title: string;
  items?: FilterItemProps[];
  values?: string[];
  onChange: (values: string[]) => void;
  showCount?: boolean;
}

const FilterItem: React.FC<
  FilterItemProps & { isSelected?: boolean; onClick: (value: string) => void; showCount?: boolean }
> = ({ value, title, onClick, count = 0, isSelected, showCount }) => (
  <li>
    <a
      onClick={() => onClick(value)}
      className={cn({ [style.selected]: isSelected })}
      role="button"
      data-testid="filterButton"
    >
      {title} {showCount && <>({count})</>}
    </a>
  </li>
);

const Filter: React.FC<FilterProps> = ({
  title,
  items = [],
  onChange,
  values = [],
  showCount = true,
}) => {
  const handleFilterClick = (value: string) => {
    if (values.includes(value)) {
      onChange(values.filter((item) => item !== value));

      return;
    }
    onChange([...values, value]);
  };

  return (
    <div className={style.filter}>
      <h4 data-testid="filterTitle" className={style.filterTitle}>
        {title}
      </h4>
      <ul className={style.filterList}>
        {items.map((filterItem) => (
          <FilterItem
            onClick={handleFilterClick}
            value={filterItem.value}
            key={filterItem.value}
            title={filterItem.title}
            count={filterItem.count}
            showCount={showCount}
            isSelected={values.includes(filterItem.value)}
          />
        ))}
      </ul>
    </div>
  );
};

export default Filter;
