import Logo from 'components/Logo';
import Search from 'components/Search';
import ShoppingCart from 'components/ShoppingCart';
import style from './header.module.scss';

const Header: React.FC = () => {
  return (
    <header className={style.header}>
      <div className={`container ${style.headerContainer}`}>
        <Logo />
        <Search />
        <ShoppingCart />
      </div>
    </header>
  );
};

export default Header;
