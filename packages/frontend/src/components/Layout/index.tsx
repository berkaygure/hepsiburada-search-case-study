import cn from 'classnames';
import { HTMLProps } from 'react';
import style from './layout.module.scss';

interface LayoutProps extends HTMLProps<HTMLDivElement> {
  hasSidebar?: boolean;
}

const Layout: React.FC<LayoutProps> & {
  Sidebar: React.FC;
  Content: React.FC<HTMLProps<HTMLDivElement>>;
  Header: React.FC;
} = ({ children, hasSidebar, className, ...props }) => (
  <div className={cn(style.layout, { [style.hasSidebar]: hasSidebar }, className)} {...props}>
    {children}
  </div>
);

Layout.Header = ({ children }) => <div className={style.layoutHeader}>{children}</div>;

Layout.Content = ({ children, ...props }) => (
  <div {...props} className={cn(style.layoutContent, props.className)}>
    {children}
  </div>
);
Layout.Sidebar = ({ children }) => <div className={style.layoutSidebar}>{children}</div>;

export default Layout;
