import { ReactComponent as HbLogo } from 'assets/logo.svg';
import { Link } from 'react-router-dom';

const Logo: React.FC = () => (
  <Link to="/">
    <HbLogo />
  </Link>
);

export default Logo;
