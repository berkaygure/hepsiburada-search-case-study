import { ReactNode } from 'react';
import style from './modal.module.scss';

export interface ModalProps {
  visible?: boolean;
  title: string;
  footer?: ReactNode;
  okText?: string;
  cancelText?: string;
  onOk?: () => void;
  onCancel?: () => void;
  showCancel?: boolean;
  children?: ReactNode;
}

const Modal: React.FC<ModalProps> = ({
  title,
  children,
  footer,
  visible = false,
  okText = 'Evet',
  cancelText = 'Hayir',
  showCancel = true,
  onOk,
  onCancel,
}) => {
  if (!visible) return null;

  return (
    <div className={style.backdrop}>
      <div className={style.modal}>
        <div className={style.header}>{title}</div>
        <div className={style.content}>{children}</div>
        <div className={style.footer}>
          {footer || (
            <>
              <button data-testid="ok" onClick={onOk} className={style.ok}>
                {okText}
              </button>
              {showCancel && (
                <button onClick={onCancel} className={style.cancel}>
                  {cancelText}
                </button>
              )}
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export default Modal;
