import { fireEvent, render, screen } from '@testing-library/react';
import { SearchContext } from 'context/SearchContext';
import PageHeader from '.';

const Wrapper: React.FC<{ search?: string; filterProducts?: () => void }> = ({
  children,
  search,
  filterProducts = jest.fn(),
}) => (
  <SearchContext.Provider
    value={{
      categoryTitle: 'iphone 11 Telefon',
      filterProducts,
      filters: [],
      query: { q: search },
      products: [],
      loading: false,
      count: 0,
      page: 1,
      total: 0,
    }}
  >
    {children}
  </SearchContext.Provider>
);

describe('TEST PageHeader', () => {
  it('should render page header', () => {
    render(<PageHeader />, { wrapper: Wrapper });
  });

  it('should render category title', () => {
    render(<PageHeader />, { wrapper: Wrapper });

    expect(screen.getByTestId('categoryTitle')).toHaveTextContent('iphone 11 Telefon');
  });

  it('should render searchQuery', () => {
    render(<PageHeader />, { wrapper: (props) => <Wrapper {...props} search="Demo" /> });

    expect(screen.getByTestId('query')).toHaveTextContent('Aranan Kelime');
    expect(screen.getByTestId('query')).toHaveTextContent('Demo');
  });

  it('should render order selection', () => {
    render(<PageHeader />, { wrapper: (props) => <Wrapper {...props} search="Demo" /> });

    expect(screen.getByRole('listbox')).toHaveTextContent('Sıralama');
  });

  it('should change order when change select', () => {
    const filter = jest.fn();
    render(<PageHeader />, {
      wrapper: (props) => <Wrapper {...props} search="Demo" filterProducts={filter} />,
    });

    fireEvent.change(screen.getByRole('listbox'));

    expect(filter).toHaveBeenCalled();
  });
});
