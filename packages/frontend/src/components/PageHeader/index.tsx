import { useSearch } from 'context/SearchContext';
import { SORT_VALUES } from 'utils/constants';
import style from './pageHeader.module.scss';

const PageHeader: React.FC = () => {
  const { query, categoryTitle, filterProducts } = useSearch();
  return (
    <div className={style.pageHeader}>
      <div>
        <h2 data-testid="categoryTitle" className={style.pageHeaderTitle}>
          {categoryTitle}
        </h2>
        <span data-testid="query" className={style.pageHeaderSearchTerm}>
          {query.q && (
            <>
              Aranan Kelime: <span>{query.q}</span>
            </>
          )}
        </span>
      </div>
      <div>
        <select
          role="listbox"
          value={query.order || 'default'}
          onChange={(e) => filterProducts('order', e.target.value)}
          placeholder="Sıralama"
        >
          <option value="default" disabled hidden>
            Sıralama
          </option>
          {SORT_VALUES.map((item) => (
            <option key={item.value} value={item.value}>
              {item.title}
            </option>
          ))}
        </select>
      </div>
    </div>
  );
};

export default PageHeader;
