import { fireEvent, render, screen } from '@testing-library/react';
import Pager from '.';
import style from './pager.module.css';

const defaultPagerProps = {
  total: 36,
  pageSize: 12,
  onChange: () => [],
  current: 1,
};

describe('TEST pager', () => {
  it('should render all pages', () => {
    render(<Pager {...defaultPagerProps} />);
    expect(screen.getAllByRole('navigation')).toHaveLength(
      Math.ceil(defaultPagerProps.total / defaultPagerProps.pageSize) + 1,
    );
  });

  it('should render custom pageSize', () => {
    render(<Pager {...defaultPagerProps} pageSize={6} />);
    expect(screen.getAllByRole('navigation')).toHaveLength(7);
  });

  it('should not select any button when current value is 0', () => {
    render(<Pager {...defaultPagerProps} current={0} />);
    screen.queryAllByRole('navigation').forEach((button) => {
      expect(button).not.toHaveClass(style.current);
    });
  });

  it('should render 0 pageSize', () => {
    render(<Pager {...defaultPagerProps} pageSize={0} />);
    expect(screen.getAllByRole('navigation')).toHaveLength(4);
  });

  it('should NOT render when total number is 0', () => {
    render(<Pager {...defaultPagerProps} total={0} current={1} />);
    expect(screen.queryAllByRole('navigation')).toHaveLength(0);
  });

  it('should render next button when current page is not last page', () => {
    render(<Pager {...defaultPagerProps} />);
    expect(screen.getByText('>')).toBeDefined();
  });

  it('should NOT render next button when current page is last page', () => {
    render(<Pager {...defaultPagerProps} current={3} />);
    expect(screen.queryAllByText('>')).toHaveLength(0);
  });

  it('should render next button when current page is not first page', () => {
    render(<Pager {...defaultPagerProps} current={2} />);
    expect(screen.getByText('<')).toBeDefined();
  });

  it('should NOT render next button when current page is first page', () => {
    render(<Pager {...defaultPagerProps} current={1} />);
    expect(screen.queryAllByText('<')).toHaveLength(0);
  });

  it('should create pages from numbers that are not exactly divisible', () => {
    render(<Pager {...defaultPagerProps} pageSize={13} total={38} />);
    expect(screen.getAllByRole('navigation')).toHaveLength(4);
  });

  it('should list all pages when pagesize grater than total', () => {
    render(<Pager {...defaultPagerProps} pageSize={100} total={38} current={1} />);
    expect(screen.queryAllByRole('navigation')).toHaveLength(0);
  });

  it('should make active the current page', () => {
    render(<Pager {...defaultPagerProps} current={2} />);
    expect(screen.getByText('2')).toHaveClass(style.current);
  });

  it('should change the page when click to a button', () => {
    const cb = jest.fn();
    render(<Pager {...defaultPagerProps} onChange={cb} />);

    fireEvent.click(screen.getByText('3'));

    expect(cb).toHaveBeenCalledWith(3);
  });

  it('should navigate to next page when next button clicked', () => {
    const cb = jest.fn();
    render(<Pager {...defaultPagerProps} onChange={cb} />);

    fireEvent.click(screen.getByText('>'));

    expect(cb).toHaveBeenCalledWith(defaultPagerProps.current + 1);
  });

  it('should navigate to prev page when prev button clicked', () => {
    const cb = jest.fn();
    render(<Pager {...defaultPagerProps} onChange={cb} current={3} />);

    fireEvent.click(screen.getByText('<'));

    expect(cb).toHaveBeenCalledWith(2);
  });
});
