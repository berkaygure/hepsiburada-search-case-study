import style from './pager.module.scss';
import cn from 'classnames';
import { HTMLProps } from 'react';

interface PagerButtonProps extends Omit<HTMLProps<HTMLButtonElement>, 'type'> {
  text: string | number;
  active?: boolean;
}

interface PagerProps {
  current: number;
  pageSize?: number;
  total: number;
  onChange: (page: number) => void;
}

const PagerButton: React.FC<PagerButtonProps> = ({ text, active = false, ...props }) => {
  return (
    <button
      role="navigation"
      data-testid="pagerButton"
      className={cn(style.pagerButton, { [style.current]: active })}
      {...props}
    >
      {text}
    </button>
  );
};

const Pager: React.FC<PagerProps> = ({ current, pageSize, total, onChange }) => {
  const totalPage = Math.ceil(total / (pageSize || 12));

  const renderButtons = () => {
    const buttons = [];
    for (let i = 1; i <= totalPage; i++) {
      buttons.push(
        <PagerButton
          key={i}
          active={current === i}
          onClick={() => onChange(i)}
          text={i}
          title={i.toString()}
        />,
      );
    }

    return buttons;
  };

  return (
    <div className={style.pager}>
      {current > 1 && total !== 0 && (
        <PagerButton onClick={() => onChange(current - 1)} text="<" title="Prev" />
      )}
      {totalPage > 1 && renderButtons()}
      {current !== totalPage && total !== 0 && (
        <PagerButton onClick={() => onChange(current + 1)} text=">" title="Next" />
      )}
    </div>
  );
};

export default Pager;
