import { render, screen, fireEvent } from '@testing-library/react';
import { ProductInterface } from 'Services/SearchService';
import { CartContextWrapper } from 'test-utils';
import Product from '.';

const productProps: { disabled: boolean; product: ProductInterface } = {
  disabled: false,
  product: {
    title: 'Demo',
    image: '/image.png',
    listPrice: 100,
    discount: 0,
    id: 1,
    discountedPrice: 0,
    attributes: [
      {
        id: 'color',
        name: 'Blue',
        title: 'Mavi',
        value: 'blue',
      },
    ],
  },
};

describe('TEST product', () => {
  it('should render product', () => {
    render(<Product {...productProps} onClick={jest.fn()} />);
    expect(screen.getByTestId('productTitle')).toHaveTextContent('Demo');
    expect(screen.getByTestId('productImage')).toHaveAttribute('src', '/image.png');
    expect(screen.getByTestId('productPrice')).toHaveTextContent('100 TL');

    productProps.product.attributes.forEach((item) => {
      expect(screen.getByTestId(item.id)).toHaveTextContent(`${item.name}: ${item.title}`);
    });
  });

  it('should show discounted price when there is a discount', () => {
    render(
      <Product
        disabled={false}
        product={{ ...productProps.product, discount: 10, discountedPrice: 90 }}
        onClick={jest.fn()}
      />,
    );
    expect(screen.getByTestId('productPrice')).toHaveTextContent('90 TL');
    expect(screen.getByTestId('discountRate')).toHaveTextContent('10%');
    expect(screen.getByTestId('listPrice')).toHaveTextContent('100 TL');
  });
  it('should show add to cart button when enabled', () => {
    render(<Product disabled={false} product={{ ...productProps.product }} onClick={jest.fn()} />);

    expect(screen.getByRole('button')).toHaveTextContent('Sepete Ekle');
    expect(screen.getByRole('button')).toHaveProperty('disabled', false);
  });

  it('should show "you cannot add to cart" button when disabled', () => {
    render(<Product disabled={true} product={{ ...productProps.product }} onClick={jest.fn()} />);

    expect(screen.getByRole('button')).toHaveTextContent('Bu ürünü sepete ekleyemezsiniz.');
    expect(screen.getByRole('button')).toHaveProperty('disabled', true);
  });
});
