import { SkeletonImage, SkeletonLine } from 'components/Skeleton';
import style from './product.module.scss';

const ProductSkeleton: React.FC = () => (
  <a className={style.product} data-testid="productLoading">
    <div className={style.productHeader}>
      <div className={style.productImage}>
        <SkeletonImage />
      </div>
    </div>
    <div className={style.productInfo}>
      <SkeletonLine width={200} />
      <SkeletonLine width={60} />

      <div className={style.productAttributes}>
        <SkeletonLine width={10} />
        <div className={style.productPrice}>
          <SkeletonLine width={50} />
        </div>
      </div>
    </div>
  </a>
);

export default ProductSkeleton;
