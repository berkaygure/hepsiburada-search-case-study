import style from './product.module.scss';
import { ProductInterface } from 'Services/SearchService';

const ProductPrice: React.FC<{ price: number }> = ({ price }) => (
  <span data-testid="productPrice" className={style.price}>
    {price} TL
  </span>
);

const ProductAttributes: React.FC<{
  discount: number;
  listPrice: number;
  discountedPrice: number;
  attributes: ProductInterface['attributes'];
}> = ({ attributes, discount, listPrice, discountedPrice }) => {
  return (
    <div className={style.productAttributes}>
      <ul>
        {attributes.map((attr) => (
          <li role="row" data-testid={attr.id} key={attr.id}>
            <strong>{attr.name}: </strong>
            <span>{attr.title}</span>
          </li>
        ))}
      </ul>
      <div className={style.productPrice}>
        <ProductPrice price={discount > 0 ? discountedPrice : listPrice} />
        {discount > 0 && (
          <span className={style.productPriceDiscount}>
            <del data-testid="listPrice">{listPrice} TL</del>
            <span data-testid="discountRate" className={style.productPriceDiscountPercent}>
              {discount}%
            </span>
          </span>
        )}
      </div>
    </div>
  );
};

const Product: React.FC<{ product: ProductInterface; disabled: boolean; onClick: () => void }> = ({
  product,
  disabled,
  onClick,
}) => {
  const { title, image, listPrice, attributes, discount, discountedPrice } = product;

  return (
    <a className={style.product} role="listitem" data-testid="product">
      <div className={style.productHeader}>
        <div className={style.productImage}>
          <img data-testid="productImage" src={image} alt={title} />
        </div>
      </div>
      <div className={style.productInfo}>
        <h4 data-testid="productTitle">{title}</h4>
        <ProductAttributes
          listPrice={listPrice}
          discount={discount}
          attributes={attributes}
          discountedPrice={discountedPrice}
        />
        <button
          role="button"
          data-testid="productButton"
          disabled={disabled}
          onClick={onClick}
          className={style.productAddCard}
        >
          {disabled ? 'Bu ürünü sepete ekleyemezsiniz.' : 'Sepete Ekle'}
        </button>
      </div>
    </a>
  );
};

export default Product;
