import { render, screen, fireEvent } from '@testing-library/react';
import { CartContextWrapper } from 'test-utils';
import { NO_PRODUCT } from 'utils/constants';
import ProductGrid from '.';

// const categoryProps = {
//   categoryTitle: 'iphone 11 Telefon',
//   filterProducts: jest.fn(),
//   filters: [],
//   query: { q: '' },

//   loading: false,
//   count: 0,
//   page: 1,
//   total: 0,
// };

const products = [
  {
    id: 1,
    image: '',
    title: 'demo',
    listPrice: 100,
    discount: 10,
    discountedPrice: 90,
    attributes: [
      {
        id: 'color',
        name: 'Color',
        title: 'Maci',
        value: 'blue',
      },
    ],
  },
  {
    id: 2,
    image: '',
    title: 'demo',
    listPrice: 100,
    discount: 0,
    discountedPrice: 0,
    attributes: [
      {
        id: 'color',
        name: 'Color',
        title: 'Kirmizi',
        value: 'blue',
      },
    ],
  },
];

describe('TEST Product Grid', () => {
  it('should render Product grid', () => {
    render(<ProductGrid products={products} loading={false} />, {
      wrapper: (props) => <CartContextWrapper {...props}>{props.children}</CartContextWrapper>,
    });

    expect(screen.getAllByRole('listitem')).toHaveLength(2);
  });

  it('should render empty when product list is empty', () => {
    render(<ProductGrid products={[]} loading={false} />, {
      wrapper: (props) => <CartContextWrapper {...props}>{props.children}</CartContextWrapper>,
    });

    expect(screen.getByText(NO_PRODUCT)).toBeDefined();
  });
  it('should add to cart', () => {
    const addCart = jest.fn();
    render(<ProductGrid products={products} loading={false} />, {
      wrapper: (props) => <CartContextWrapper {...props} addToCart={addCart} />,
    });

    fireEvent.click(screen.getAllByRole('button')[0]);

    expect(addCart).toBeCalledTimes(1);
  });

  it('should render loading', () => {
    render(<ProductGrid products={products} loading={true} />, {
      wrapper: (props) => <CartContextWrapper {...props}>{props.children}</CartContextWrapper>,
    });

    expect(screen.getAllByTestId('productLoading')).toHaveLength(8);
  });
});
