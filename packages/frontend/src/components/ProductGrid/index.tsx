import Product from 'components/Product';
import ProductSkeleton from 'components/Product/ProductSkeleton';
import { useCart } from 'context/CartContext';
import type { ProductInterface } from 'Services/SearchService';
import { NO_PRODUCT } from 'utils/constants';
import style from './productGrid.module.scss';

const EmptyGrid: React.FC = () => <div>{NO_PRODUCT}</div>;

const ProductLoading = () => (
  <>
    {Array.from(Array(8).keys()).map((e) => (
      <ProductSkeleton key={`loading${e}`} />
    ))}
  </>
);

const ProductGrid: React.FC<{ products: ProductInterface[]; loading: boolean }> = ({
  products,
  loading,
}) => {
  const { hasInCart, addToCart } = useCart();

  return (
    <div data-testid="productList" role="list" className={style.container}>
      {loading ? (
        <ProductLoading />
      ) : (
        products.map((product) => (
          <Product
            key={product.id}
            product={product}
            disabled={hasInCart(product.id)}
            onClick={() =>
              addToCart({ id: product.id, image: product.image, title: product.title })
            }
          />
        ))
      )}
      {!loading && products.length === 0 && <EmptyGrid />}
    </div>
  );
};

export default ProductGrid;
