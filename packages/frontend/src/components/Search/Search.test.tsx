import { render, screen, fireEvent } from '@testing-library/react';
import { SearchContextWrapper } from 'test-utils';
import { DEFAULT_SEARCH_PLACEHOLDER } from 'utils/constants';
import Search from '.';

describe('Search', () => {
  it('should render search', () => {
    render(<Search />, { wrapper: (props) => <SearchContextWrapper {...(props as any)} /> });
    expect(screen.getByPlaceholderText(DEFAULT_SEARCH_PLACEHOLDER)).toBeDefined();
  });

  it('should NOT search when textlength less than 2', () => {
    const mockFilter = jest.fn();
    render(<Search />, {
      wrapper: (props) => <SearchContextWrapper {...(props as any)} filterProducts={mockFilter} />,
    });

    fireEvent.change(screen.getByPlaceholderText(DEFAULT_SEARCH_PLACEHOLDER), {
      target: { value: 'BG' },
    });

    expect(mockFilter).toHaveBeenCalledTimes(0);
  });

  it('should search when textlength grater than 2', () => {
    jest.useFakeTimers();
    const mockFilter = jest.fn();
    render(<Search />, {
      wrapper: (props) => <SearchContextWrapper {...(props as any)} filterProducts={mockFilter} />,
    });

    fireEvent.change(screen.getByRole('searchbox'), {
      target: { value: 'Berkay' },
    });
    jest.runAllTimers();

    expect(mockFilter).toHaveBeenCalledTimes(1);
  });

  it('should search when textlength equals 0', () => {
    jest.useFakeTimers();

    const mockFilter = jest.fn();
    render(<Search />, {
      wrapper: (props) => <SearchContextWrapper {...(props as any)} filterProducts={mockFilter} />,
    });

    fireEvent.change(screen.getByRole('searchbox'), {
      target: { value: 'Bg' },
    });
    fireEvent.change(screen.getByRole('searchbox'), {
      target: { value: '' },
    });
    jest.runAllTimers();

    expect(mockFilter).toHaveBeenCalledTimes(1);
  });
});
