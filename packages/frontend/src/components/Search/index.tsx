import { ReactComponent as SearchIcon } from 'assets/search.svg';
import { useSearch } from 'context/SearchContext';
import { debounce } from 'utils';
import { DEFAULT_SEARCH_PLACEHOLDER } from 'utils/constants';
import style from './search.module.scss';

type SearchProps = { placeholder?: string };

const Search: React.FC<SearchProps> = ({ placeholder = DEFAULT_SEARCH_PLACEHOLDER }) => {
  const { filterProducts, query } = useSearch();

  const debounceFilter = debounce((e: React.ChangeEvent<HTMLInputElement>) => {
    const keyword = e.target.value;
    if (keyword.length === 0 || keyword.length > 2) {
      filterProducts('q', e.target.value);
    }
  });

  return (
    <div className={style.search}>
      <span className={style.searchIcon}>
        <SearchIcon />
      </span>

      <span className={style.searchInput}>
        <input
          role="searchbox"
          type="text"
          defaultValue={query.q}
          placeholder={placeholder}
          onChange={debounceFilter}
        />
      </span>
    </div>
  );
};

export default Search;
