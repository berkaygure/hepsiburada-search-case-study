import style from './shopping.module.scss';
import { CartItem } from 'context/CartContext';

const Product: React.FC<{ item: CartItem; onRemove: (id: number) => void }> = ({
  item,
  onRemove,
}) => (
  <li role="listitem" className={style.cardProduct}>
    <div className={style.cardProductImage}>
      <img src={item.image} alt={item.title} />
    </div>
    <div className={style.cardProductDetail}>
      <a href="/" className={style.cardProductDetailTitle}>
        {item.title}
      </a>
      <div>
        <button
          data-testid={`removeCart_${item.id}`}
          onClick={() => onRemove(item.id)}
          className={style.cardProductButton}
        >
          Kaldir
        </button>
      </div>
    </div>
  </li>
);

export default Product;
