import { render, screen, fireEvent } from '@testing-library/react';
import { CartContextWrapper } from 'test-utils';
import { CART_EMPTY_MESSAGE } from 'utils/constants';
import ShoppingCart from '.';

const cartItems = [
  {
    id: 1,
    title: 'Demo',
    image: 'test',
    created_at: new Date(),
  },
  {
    id: 2,
    title: 'Demo old',
    image: 'test1',
    created_at: new Date('2021-09-07'),
  },
  {
    id: 3,
    title: 'Demo old 2',
    image: 'test2',
    created_at: new Date('2021-09-08'),
  },
];

describe('TEST ShoppingCart', () => {
  it('Should render empty message when list is empty', () => {
    render(<ShoppingCart />, { wrapper: (props) => <CartContextWrapper {...props} items={[]} /> });
    expect(screen.queryByText(CART_EMPTY_MESSAGE)).toBeDefined();
  });

  it('Should render list', () => {
    render(<ShoppingCart />, {
      wrapper: (props) => <CartContextWrapper {...props} items={cartItems} />,
    });

    expect(screen.queryAllByRole('listitem')).toHaveLength(3);
  });

  it('Should show cart items count', () => {
    render(<ShoppingCart />, {
      wrapper: (props) => <CartContextWrapper {...props} items={cartItems} />,
    });

    expect(screen.getByTestId('cartBadge')).toHaveTextContent(cartItems.length.toString());
  });

  it('Should remove from list', () => {
    const remove = jest.fn();
    render(<ShoppingCart />, {
      wrapper: (props) => (
        <CartContextWrapper {...props} items={cartItems} removeFromCart={remove} />
      ),
    });

    fireEvent.click(screen.getByTestId('removeCart_3'));
    fireEvent.click(screen.getByTestId('ok'));

    expect(remove).toBeCalledTimes(1);
  });
});
