import style from './shopping.module.scss';
import { useCart } from 'context/CartContext';
import Product from './Product';
import Dialog from 'components/Dialog';
import { Link } from 'react-router-dom';
import { CART_EMPTY_MESSAGE } from 'utils/constants';

const ShoppingCart: React.FC = () => {
  const { items, removeFromCart } = useCart();

  const handleRemove = (id: number) => {
    Dialog.confirm({
      title: 'Ürünü silmek istediğinize emin misiniz?',
      children: 'Seçtiğiniz ürün sepetten çıkarılacaktır. Onaylıyor musunuz?',
      onOk: () => {
        removeFromCart(id);
      },
    });
  };

  return (
    <div className={style.card}>
      <div className={style.cardHandler}>
        Sepetim
        <span data-testid="cartBadge" className={style.cardBadge}>
          {items.length}
        </span>
      </div>
      <div className={style.cardDropdown}>
        <ul>
          {items
            .sort((a, b) => (a.created_at as never) - (b.created_at as never))
            .map((item) => (
              <Product key={item.id} item={item} onRemove={handleRemove} />
            ))}
        </ul>
        {items.length === 0 && (
          <Link to="/" className={style.empty}>
            {CART_EMPTY_MESSAGE}
          </Link>
        )}
      </div>
    </div>
  );
};

export default ShoppingCart;
