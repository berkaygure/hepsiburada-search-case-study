import style from './skeleton.module.scss';

const SkeletonImage: React.FC = () => <div className={style.skeletonImage} />;

export default SkeletonImage;
