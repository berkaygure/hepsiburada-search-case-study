import style from './skeleton.module.scss';

const SkeletonLine: React.FC<{ width?: number }> = ({ width }) => (
  <span style={{ width }} className={style.skeletonLine} />
);

export default SkeletonLine;
