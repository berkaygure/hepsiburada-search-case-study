import { useEffect, useState, useCallback, useContext, createContext } from 'react';

export interface CartItem {
  id: number;
  title: string;
  image: string;
  created_at?: Date;
}

interface CartContextInterface {
  items: CartItem[];
  addToCart: (item: CartItem) => void;
  removeFromCart: (id: number) => void;
  hasInCart: (id: number) => boolean;
}

const CartContext = createContext<CartContextInterface | undefined>(undefined);

const CartContextProvider: React.FC = ({ children }) => {
  const [cart, setCart] = useState<CartItem[]>(JSON.parse(localStorage.getItem('cart') || '[]'));

  useEffect(() => {
    localStorage.setItem('cart', JSON.stringify(cart));
  }, [cart]);

  const hasInCart = useCallback((id: number) => cart.findIndex((x) => x.id === id) > -1, [cart]);

  const addToCart = useCallback(
    (item: CartItem) => {
      if (!hasInCart(item.id)) {
        setCart([...cart, { ...item, created_at: new Date() }]);
      }
    },
    [cart],
  );

  const removeFromCart = useCallback(
    (id: number) => {
      if (hasInCart(id)) {
        setCart(cart.filter((c) => c.id !== id));
      }
    },
    [cart],
  );

  return (
    <CartContext.Provider value={{ items: cart, addToCart, hasInCart, removeFromCart }}>
      {children}
    </CartContext.Provider>
  );
};

export const useCart = (): CartContextInterface => {
  const cartContext = useContext(CartContext);

  if (cartContext === undefined) {
    throw new Error('You cant use this hook without a provider.');
  }

  return cartContext;
};

export default CartContextProvider;

export { CartContext };
