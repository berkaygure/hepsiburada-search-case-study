import { queryToString } from 'utils';
import { useHistory, useLocation } from 'react-router';
import { useState, createContext, useContext, useEffect } from 'react';

import { filterProductsAPI, FilterResult, ProductFilterQuery } from 'Services/SearchService';

export interface SearchContextInterface extends FilterResult {
  filterProducts: (key: keyof ProductFilterQuery, value: string | number) => void;
  query: ProductFilterQuery;
  loading: boolean;
}

const SearchContext = createContext<SearchContextInterface | undefined>(undefined);

const SearchProvider: React.FC = ({ children }) => {
  const { push } = useHistory();
  const { search } = useLocation();
  const [query, setQuery] = useState<ProductFilterQuery>({});
  const [loading, setLoading] = useState(false);
  const [result, setResult] = useState<FilterResult>({
    categoryTitle: '',
    filters: [],
    products: [],
    page: 1,
    count: 0,
    total: 0,
  });

  useEffect(() => {
    if (search) {
      const params = new URLSearchParams(search);
      setQuery({
        q: params.get('q') || '',
        filters: params.get('filters') || '',
        order: params.get('order') || '',
        page: (params.get('page') || 0) as number,
      });
    } else {
      setQuery({});
    }
  }, [search]);

  useEffect(() => {
    setLoading(true);
    filterProductsAPI(search)
      .then(setResult)
      .catch(() => {
        // report error
      })
      .finally(() => {
        setLoading(false);
      });
  }, [query]);

  const filterProducts = (key: keyof ProductFilterQuery, value: string | number) =>
    push({
      search: queryToString({ ...query, page: 0, [key]: value }),
    });

  return (
    <SearchContext.Provider value={{ ...result, filterProducts, query, loading }}>
      {children}
    </SearchContext.Provider>
  );
};

const useSearch = (): SearchContextInterface => {
  const context = useContext(SearchContext);
  if (context === undefined) {
    throw new Error('You cant use this hook without a provider.');
  }
  return context;
};

export { SearchProvider, useSearch, SearchContext };
