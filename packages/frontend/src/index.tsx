import ReactDOM from 'react-dom';
import HepsiburadaSearch from './HepsiburadaSearch';

import './index.css';

ReactDOM.render(<HepsiburadaSearch />, document.getElementById('root'));
