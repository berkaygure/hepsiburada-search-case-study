import { render, screen, fireEvent } from '@testing-library/react';
import { CartContextWrapper, SearchContextWrapper } from 'test-utils';
import ProductSearch from './ProductSearch';

const filters = [
  {
    id: 'color',
    title: 'Renk',
    items: [
      {
        value: 'blue',
        title: 'Mavi',
      },
    ],
  },
  {
    id: 'brand',
    title: 'Marka',
    items: [
      {
        value: 'apple',
        title: 'Apple',
      },
    ],
  },
];

const products = [
  {
    id: 1,
    image: '',
    title: 'iphone 11',
    listPrice: 100,
    discount: 10,
    discountedPrice: 90,
    attributes: [
      {
        id: 'color',
        name: 'Color',
        title: 'Mavi',
        value: 'blue',
      },
    ],
  },
  {
    id: 2,
    image: '',
    title: 'demo',
    listPrice: 100,
    discount: 0,
    discountedPrice: 0,
    attributes: [
      {
        id: 'color',
        name: 'Color',
        title: 'Kirmizi',
        value: 'blue',
      },
    ],
  },
];

describe('TEST Product Search', () => {
  it('should render page', () => {
    render(<ProductSearch />, {
      wrapper: (props) => (
        <CartContextWrapper>
          <SearchContextWrapper {...(props as any)} products={products} filters={filters}>
            {props.children}
          </SearchContextWrapper>
        </CartContextWrapper>
      ),
    });

    expect(screen.getByText('Siralama')).toBeDefined();
    expect(screen.getByText('Renk')).toBeDefined();
    expect(screen.getByText('Marka')).toBeDefined();
    expect(screen.getByText('iphone 11')).toBeDefined();
    expect(screen.getAllByTestId('product')).toHaveLength(2);
  });

  it('should filter data ', () => {
    const mockFn = jest.fn();
    render(<ProductSearch />, {
      wrapper: (props) => (
        <CartContextWrapper>
          <SearchContextWrapper
            {...(props as any)}
            products={[]}
            filters={filters}
            filterProducts={mockFn}
          >
            {props.children}
          </SearchContextWrapper>
        </CartContextWrapper>
      ),
    });
    const filterBtns = screen.getAllByTestId('filterButton');
    screen.getAllByTestId('filterButton').forEach((btn) => {
      fireEvent.click(btn);
    });

    expect(mockFn).toBeCalledTimes(filterBtns.length);
  });

  it('should paginate data ', () => {
    const mockFn = jest.fn();
    render(<ProductSearch />, {
      wrapper: (props) => (
        <CartContextWrapper>
          <SearchContextWrapper
            {...(props as any)}
            products={products}
            filters={filters}
            filterProducts={mockFn}
            total={24}
            page={1}
            count={2}
          >
            {props.children}
          </SearchContextWrapper>
        </CartContextWrapper>
      ),
    });

    fireEvent.click(screen.getAllByTestId('pagerButton')[0]);

    expect(mockFn).toBeCalledTimes(1);
  });
});
