import { useState, useEffect } from 'react';
import Layout from 'components/Layout';
import Filter from 'components/Filter';
import { parseFilterQuery, createFilterString } from 'utils';
import PageHeader from 'components/PageHeader';
import ProductGrid from 'components/ProductGrid';
import { useSearch } from 'context/SearchContext';
import Pager from 'components/Pager';
import { SORT_VALUES, PAGE_SIZE } from 'utils/constants';

const ProductSearch: React.FC = () => {
  const { products, filters, query, filterProducts, loading, count, total } = useSearch();
  const [filteredValue, setFilteredValue] = useState<Record<string, string[]>>(
    parseFilterQuery(query.filters),
  );

  useEffect(() => {
    setFilteredValue(parseFilterQuery(query.filters));
  }, [query.filters]);

  const handleFilterChange = (filter: string, items: string[]) => {
    filterProducts('filters', createFilterString({ ...filteredValue, [filter]: items }));
  };

  return (
    <Layout.Content className="container">
      <PageHeader />
      <Layout hasSidebar>
        <Layout.Sidebar>
          <Filter
            key="order"
            values={query.order ? [query.order] : []}
            title="Siralama"
            items={SORT_VALUES}
            showCount={false}
            onChange={(items) => filterProducts('order', items[items.length - 1])}
          />
          {filters.map((filter) => (
            <Filter
              key={filter.id}
              values={filteredValue[filter.id]}
              title={filter.title}
              items={filter.items}
              showCount
              onChange={(items) => handleFilterChange(filter.id, items)}
            />
          ))}
        </Layout.Sidebar>
        <Layout.Content>
          <ProductGrid products={products} loading={loading} />
          {count > 0 && (
            <Pager
              total={total}
              pageSize={PAGE_SIZE}
              current={Number(query.page || '1')}
              onChange={(page) => filterProducts('page', page)}
            />
          )}
        </Layout.Content>
      </Layout>
    </Layout.Content>
  );
};

export default ProductSearch;
