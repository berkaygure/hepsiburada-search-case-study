import React, { ReactElement } from 'react';
import { render, RenderOptions, RenderResult } from '@testing-library/react';
import CartContextProvider, { CartContext, CartItem } from 'context/CartContext';
import { SearchContext, SearchContextInterface, SearchProvider } from 'context/SearchContext';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

const AllTheProviders: React.FC = ({ children }) => {
  return (
    <Router>
      <CartContextProvider>
        <SearchProvider>
          <Switch>
            <Route>{children}</Route>
          </Switch>
        </SearchProvider>
      </CartContextProvider>
    </Router>
  );
};

const customRender = (ui: ReactElement, options?: RenderOptions): RenderResult =>
  render(ui, { wrapper: AllTheProviders, ...options });

export const CartContextWrapper: React.FC<{
  addToCart?: (item: CartItem) => void;
  removeFromCart?: (id: number) => void;
  hasInCart?: (id: number) => boolean;
  items?: CartItem[];
}> = ({
  addToCart = jest.fn(),
  removeFromCart = jest.fn(),
  hasInCart = jest.fn(),
  items = [],
  children,
}) => (
  <Router>
    <Switch>
      <CartContext.Provider value={{ addToCart, removeFromCart, hasInCart, items }}>
        {children}
      </CartContext.Provider>
    </Switch>
  </Router>
);

export const SearchContextWrapper: React.FC<SearchContextInterface> = ({
  categoryTitle = '',
  count = 0,
  filterProducts = jest.fn(),
  filters = [],
  loading = false,
  page = 1,
  products = [],
  query = {},
  total = 0,
  children,
}) => (
  <Router>
    <Switch>
      <SearchContext.Provider
        value={{
          categoryTitle,
          count,
          filters,
          filterProducts,
          total,
          loading,
          query,
          products,
          page,
        }}
      >
        {children}
      </SearchContext.Provider>
    </Switch>
  </Router>
);

export * from '@testing-library/react';
export { customRender as render };
