export const PAGE_SIZE = 12;
export const CART_EMPTY_MESSAGE = 'Sepetin Boş, hemen alışverişe başla';
export const DEFAULT_SEARCH_PLACEHOLDER = '25 milyon’dan fazla ürün içerisinde ara';
export const NO_PRODUCT = 'Hic urun bulamadik :(';

export const SORT_VALUES = [
  {
    value: 'price:ASC',
    title: 'En Düşük Fiyat',
  },
  {
    value: 'price:DESC',
    title: 'En Yüksek Fiyat',
  },
  {
    value: 'title:ASC',
    title: 'En Yeniler (A>Z)',
  },
  {
    value: 'title:DESC',
    title: 'En Yeniler (Z>A)',
  },
];
