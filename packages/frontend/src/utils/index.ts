/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */

export const debounce = (fn: any, ms = 300): (() => void) => {
  let timeoutId: ReturnType<typeof setTimeout>;
  return function (this: any, ...args: any[]) {
    clearTimeout(timeoutId);
    timeoutId = setTimeout(() => fn.apply(this, args), ms);
  };
};

export const queryToString = (filter: Record<string, any>): string => {
  return Object.entries(filter).reduce((prev, [key, val]) => {
    if (val && typeof val !== 'object') {
      return `${prev}&${key}=${val}`;
    }
    return prev;
  }, '');
};

export const parseFilterQuery = (filterQuery?: string): Record<string, string[]> => {
  if (!filterQuery) return {};
  const filters = filterQuery.split(';');
  const filterObject: Record<string, string[]> = {};
  filters.forEach((filter) => {
    const [name, value] = filter.split(':');

    if (value) {
      filterObject[name] = value.split(',');
    }
  });

  return filterObject;
};

export const createFilterString = (filter: Record<string, string[]>) =>
  Object.entries(filter).reduce((str, [key, value]) => {
    if (value.length > 0) {
      return `${str}${key}:${value.join(',')};`;
    }
    return str;
  }, '');
