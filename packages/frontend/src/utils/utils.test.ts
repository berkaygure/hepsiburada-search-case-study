import { debounce, queryToString, parseFilterQuery, createFilterString } from 'utils';

describe('utils tests', () => {
  describe('TEST filterToString', () => {
    const obj = {
      name: 'berkay',
      surname: 'gure',
      age: 27,
    };

    const objWithFalsy = {
      ...obj,
      location: false,
      address: '',
      website: null,
      twitter: undefined,
      demo: [],
      demo2: {},
    };

    it('should convert to string given object', () => {
      const result = queryToString(obj);
      expect(result).toBe('&name=berkay&surname=gure&age=27');
    });

    it('should not falsy valued key to string', () => {
      const result = queryToString(objWithFalsy);
      expect(result).toBe('&name=berkay&surname=gure&age=27');
    });

    it('should convert empty object to blank string', () => {
      const result = queryToString({});
      expect(result).toBe('');
    });
  });

  describe('TEST parseFilterQuery', () => {
    const stringfyObject = 'name:berkay;surname:gure;hobbies:book,games,movies;';
    const emptyStringfyObject = `${stringfyObject}projects:`;

    it('should parse to object the given string', () => {
      const result = parseFilterQuery(stringfyObject);

      expect(result).toStrictEqual({
        name: ['berkay'],
        surname: ['gure'],
        hobbies: ['book', 'games', 'movies'],
      });
    });

    it("should NOT parse to object when given object's key is empty", () => {
      const result = parseFilterQuery(emptyStringfyObject);

      expect(result).toStrictEqual({
        name: ['berkay'],
        surname: ['gure'],
        hobbies: ['book', 'games', 'movies'],
      });
    });

    it('should parse empty query to parse empty object', () => {
      const result = parseFilterQuery('');

      expect(result).toStrictEqual({});
    });
  });

  describe('TEST createFilterString', () => {
    it('should parse object to string', () => {
      const filter = {
        color: ['blue'],
        brand: ['apple', 'samsung'],
      };
      const result = createFilterString(filter);
      expect(result).toEqual('color:blue;brand:apple,samsung;');
    });
  });

  describe('TEST debounce', () => {
    jest.useFakeTimers();

    const callback = jest.fn();
    const testDebounce = debounce(callback, 1000);
    testDebounce();

    expect(callback).not.toBeCalled();

    jest.runTimersToTime(1000);

    expect(callback).toBeCalled();
    expect(callback).toHaveBeenCalledTimes(1);
  });

  describe('TEST debounce default 300 ms', () => {
    jest.useFakeTimers();

    const callback = jest.fn();
    const testDebounce = debounce(callback);
    testDebounce();

    expect(callback).not.toBeCalled();

    jest.runTimersToTime(300);

    expect(callback).toBeCalled();
    expect(callback).toHaveBeenCalledTimes(1);
  });
});
