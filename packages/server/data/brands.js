module.exports = [
  {
    value: "samsung",
    title: "Samsung",
  },
  {
    value: "nokia",
    title: "Nokia",
  },
  {
    value: "apple",
    title: "Apple",
  },
  {
    value: "lg",
    title: "LG",
  },
  {
    value: "huawei",
    title: "Huawei",
  },
  {
    value: "xiaomi",
    title: "Xiaomi",
  },
  {
    value: "general_mobile",
    title: "General Mobile",
  },
];
