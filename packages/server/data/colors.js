module.exports = [
  {
    value: "blue",
    title: "Mavi",
  },
  {
    value: "red",
    title: "Kırmızı",
  },
  {
    value: "black",
    title: "Siyah",
  },
  {
    value: "white",
    title: "Beyaz",
  },
];
