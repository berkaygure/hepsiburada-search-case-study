const brands = require("./brands");
const colors = require("./colors");

module.exports = [
  {
    id: "color",
    title: "Renk",
    items: colors,
  },
  {
    id: "brand",
    title: "Marka",
    items: brands,
  },
];
