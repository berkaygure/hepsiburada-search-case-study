const faker = require("faker");
const brands = require("../data/brands");
const colors = require("../data/colors");

const fileAddress = `${process.env.APP_URL}/static/`;

const generateData = (count) => {
  const dataset = [];

  for (let i = 0; i < count; i++) {
    const discountRate = faker.helpers.randomize([0, 12, 15, 20, 40, 50]);
    const listPrice = parseFloat(faker.commerce.price());
    const discount = (listPrice * discountRate) / 100;

    dataset.push({
      id: i + 1,
      title: faker.commerce.productName(),
      image: `${fileAddress}${faker.datatype.number({ min: 1, max: 4 })}.png`,
      listPrice,
      discount: discountRate,
      discountedPrice: listPrice - discount,
      attributes: [
        {
          id: "color",
          name: "Renk",
          ...faker.helpers.randomize(colors),
        },
        {
          id: "brand",
          name: "Marka",
          ...faker.helpers.randomize(brands),
        },
      ],
    });
  }

  return dataset;
};

module.exports = { generateData };
