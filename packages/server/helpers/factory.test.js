const { generateData } = require("./factory");

describe("TEST factory", () => {
  it("should be generate products", () => {
    const data = generateData(46);
    const dataOne = generateData(1);

    expect(data).toHaveLength(46);
    expect(dataOne).toHaveLength(1);
  });
});
