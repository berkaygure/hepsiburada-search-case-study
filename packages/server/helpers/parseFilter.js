const parseFilterQuery = (filterQuery) => {
  if (!filterQuery) return [];
  const filters = filterQuery.split(";");
  const filterResult = [];

  filters.forEach((filter) => {
    const [name, value] = filter.split(":");
    if (value) {
      filterResult.push({
        id: name,
        values: value.split(","),
      });
    }
  });

  return filterResult;
};

module.exports = { parseFilterQuery };
