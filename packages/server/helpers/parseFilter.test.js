const { parseFilterQuery } = require("./parseFilter");

describe("TEST parseFilter", () => {
  it("should parse given filters are not empty", () => {
    const filter = "color:red;brand:apple,orange";
    const result = parseFilterQuery(filter);
    expect(result).toStrictEqual([
      {
        id: "color",
        values: ["red"],
      },
      {
        id: "brand",
        values: ["apple", "orange"],
      },
    ]);
  });

  it("should parse empty filter", () => {
    const filter = "";
    const result = parseFilterQuery(filter);
    expect(result).toStrictEqual([]);
  });

  it("should not parse empty valued filter", () => {
    const filter = "color:;brand:apple;";
    const result = parseFilterQuery(filter);
    expect(result).toStrictEqual([
      {
        id: "brand",
        values: ["apple"],
      },
    ]);
  });
});
