const parseOrder = (order) => {
  if (!order) return { column: "id", direction: "ASC" };

  const [column, direction] = order.split(":");

  return {
    column: column === "price" ? "discountedPrice" : column,
    direction,
  };
};

module.exports = {
  parseOrder,
};
