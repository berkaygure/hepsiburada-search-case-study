const { parseOrder } = require("./parseOrder");

describe("TEST parseOrder", () => {
  it("should parse given order", () => {
    const filter = "title:DESC";
    const result = parseOrder(filter);

    expect(result).toStrictEqual({
      column: "title",
      direction: "DESC",
    });
  });

  it("should get default order when order empty", () => {
    const filter = "";
    const result = parseOrder(filter);
    expect(result).toStrictEqual({
      column: "id",
      direction: "ASC",
    });
  });

  it("should convert price filed to discountedPrice", () => {
    const filter = "price:DESC";
    const result = parseOrder(filter);
    expect(result).toStrictEqual({
      column: "discountedPrice",
      direction: "DESC",
    });
  });
});
