require("dotenv").config();

const Koa = require("koa");
const Router = require("@koa/router");
const cors = require("@koa/cors");
const serve = require("koa-static");
const mount = require("koa-mount");
const bodyParser = require("koa-bodyparser");

const Facet = require("./service/Facet");
const { generateData } = require("./helpers/factory");
const filters = require("./data/filters");
const { parseFilterQuery } = require("./helpers/parseFilter");
const { search, order, paginate } = require("./service/service");
const { parseOrder } = require("./helpers/parseOrder");

const products = generateData(36);

const app = new Koa();
const router = new Router();
const facet = new Facet({ filters });

// Set-up middleware
app.use(bodyParser());
app.use(cors());
app.use(router.routes());
app.use(router.allowedMethods());
app.use(mount("/static", serve(__dirname + "/static")));

// Filtering and Search Endpoint
router.get("/search", async (ctx) => {
  const params = ctx.request.query;
  const page = params.page || 1;
  const searchKeyword = params.q;
  const orderOptions = parseOrder(params.order);
  const filters = parseFilterQuery(params.filters);

  const { filter, data } = facet.execute(
    search(products, searchKeyword),
    filters
  );

  const paginatedData = paginate(
    order(data, orderOptions.column, orderOptions.direction),
    page
  );

  ctx.body = {
    products: paginatedData,
    filters: filter,
    total: data.length,
    page: params.page,
    count: paginatedData.length,
    categoryTitle: "iPhone 11 Telefon Modelleri",
  };
});

// Listen port
const port = process.env.PORT || 3001;
app.listen(port, () => {
  console.log(`listening server on ${process.env.HOST}:${port}`);
});
