class Facet {
  #filters = [];
  #searchKey = "attributes";
  #filterCountMap = new Map();

  constructor({ filters, searchingKey = "attributes" }) {
    this.#searchKey = searchingKey;
    this.#filters = filters;
    this.#reset();
  }

  /**
   *  Filter
   * @param {Object[]} dataset
   * @param {Object[]} filters - Filters to apply
   * @param {string} filters[].id - Filter id
   * @param {string} employees[].values - Filtered values
   * @returns {Object} result - Fitler result
   * @returns {Object[]} data - filtered data
   * @returns {Object[]} filters - filters
   */
  execute(dataset, filters = []) {
    this.#reset();
    const data = this.#filterProducts(dataset, filters);
    this.#countFilter(data);

    return {
      filter: this.#filters.map((filter) => ({
        ...filter,
        items: filter.items.map((fi) => ({
          ...fi,
          count: this.#filterCountMap.get(filter.id).get(fi.value),
        })),
      })),
      data,
    };
  }

  #countFilter(dataset) {
    dataset.forEach((data) => {
      const filterArr = data[this.#searchKey];
      filterArr.forEach((attr) => {
        const filter = this.#filterCountMap.get(attr.id);
        const count = filter.get(attr.value);
        filter.set(attr.value, count + 1);
      });
    });
  }

  #filterProducts(dataset, filters) {
    return dataset.filter((p) => {
      return filters.every((f) => {
        return p[this.#searchKey].some(
          (a) => a.id === f.id && f.values.includes(a.value)
        );
      });
    });
  }

  #reset() {
    // Fill the filter map with 0 count
    this.#filterCountMap = new Map(
      this.#filters.map((fl) => [
        fl.id,
        new Map(fl.items.map((i) => [i.value, 0])),
      ])
    );
  }
}

module.exports = Facet;
