const filters = require("../data/filters");
const Facet = require("./Facet");
const { generateData } = require("../helpers/factory");

const allData = [
  {
    id: 1,
    title: "Incredible Wooden Car",
    image: "undefined/static/1.png",
    listPrice: 598,
    discount: 12,
    discountedPrice: 526.24,
    attributes: [
      { id: "color", name: "Renk", value: "red", title: "Kırmızı" },
      { id: "brand", name: "Marka", value: "xiaomi", title: "Xaomi" },
    ],
  },
  {
    id: 2,
    title: "Intelligent Cotton Soap",
    image: "undefined/static/2.png",
    listPrice: 859,
    discount: 12,
    discountedPrice: 755.92,
    attributes: [
      { id: "color", name: "Renk", value: "blue", title: "Mavi" },
      { id: "brand", name: "Marka", value: "nokia", title: "Nokia" },
    ],
  },
  {
    id: 3,
    title: "Incredible Rubber Hat",
    image: "undefined/static/2.png",
    listPrice: 67,
    discount: 20,
    discountedPrice: 53.6,
    attributes: [
      { id: "color", name: "Renk", value: "blue", title: "Mavi" },
      { id: "brand", name: "Marka", value: "huawei", title: "Huawei" },
    ],
  },
  {
    id: 4,
    title: "Small Granite Chips",
    image: "undefined/static/4.png",
    listPrice: 231,
    discount: 12,
    discountedPrice: 203.28,
    attributes: [
      { id: "color", name: "Renk", value: "red", title: "Kırmızı" },
      {
        id: "brand",
        name: "Marka",
        value: "samsung",
        title: "Samsung",
      },
    ],
  },
  {
    id: 5,
    title: "Ergonomic Rubber Car",
    image: "undefined/static/3.png",
    listPrice: 148,
    discount: 20,
    discountedPrice: 118.4,
    attributes: [
      { id: "color", name: "Renk", value: "white", title: "Beyaz" },
      {
        id: "brand",
        name: "Marka",
        value: "samsung",
        title: "Samsung",
      },
    ],
  },
];

describe("TEST Facet", () => {
  it("should return all without filter", () => {
    const facet = new Facet({ filters });

    const { data, filter } = facet.execute(allData);

    expect(data).toHaveLength(allData.length);
    expect(filter).toStrictEqual([
      {
        id: "color",
        title: "Renk",
        items: [
          {
            title: "Mavi",
            value: "blue",
            count: 2,
          },
          {
            title: "Kırmızı",
            value: "red",
            count: 2,
          },
          {
            title: "Siyah",
            value: "black",
            count: 0,
          },
          {
            title: "Beyaz",
            value: "white",
            count: 1,
          },
        ],
      },
      {
        id: "brand",
        title: "Marka",
        items: [
          {
            value: "samsung",
            title: "Samsung",
            count: 2,
          },
          {
            value: "nokia",
            title: "Nokia",
            count: 1,
          },
          {
            value: "apple",
            title: "Apple",
            count: 0,
          },
          {
            value: "lg",
            title: "LG",
            count: 0,
          },
          {
            value: "huawei",
            title: "Huawei",
            count: 1,
          },
          {
            value: "xiaomi",
            title: "Xiaomi",
            count: 1,
          },
          {
            value: "general_mobile",
            title: "General Mobile",
            count: 0,
          },
        ],
      },
    ]);
  });

  it("should filter correctly", () => {
    const facet = new Facet({ filters });

    const { data, filter } = facet.execute(allData, [
      {
        id: "color",
        values: ["blue"],
      },
    ]);

    expect(data).toHaveLength(2);
    expect(filter).toStrictEqual([
      {
        id: "color",
        title: "Renk",
        items: [
          {
            title: "Mavi",
            value: "blue",
            count: 2,
          },
          {
            title: "Kırmızı",
            value: "red",
            count: 0,
          },
          {
            title: "Siyah",
            value: "black",
            count: 0,
          },
          {
            title: "Beyaz",
            value: "white",
            count: 0,
          },
        ],
      },
      {
        id: "brand",
        title: "Marka",
        items: [
          {
            value: "samsung",
            title: "Samsung",
            count: 0,
          },
          {
            value: "nokia",
            title: "Nokia",
            count: 1,
          },
          {
            value: "apple",
            title: "Apple",
            count: 0,
          },
          {
            value: "lg",
            title: "LG",
            count: 0,
          },
          {
            value: "huawei",
            title: "Huawei",
            count: 1,
          },
          {
            value: "xiaomi",
            title: "Xiaomi",
            count: 0,
          },
          {
            value: "general_mobile",
            title: "General Mobile",
            count: 0,
          },
        ],
      },
    ]);
  });
});
