const { RECORD_PER_PAGE } = require("../helpers/constant");

function paginate(data, page, total = RECORD_PER_PAGE) {
  return data.slice((page - 1) * total, page * total);
}

function search(data, keyword) {
  if (!keyword) return data;

  const q = keyword.toString().trim().toLowerCase();

  return data.filter((item) => {
    const title = item.title.toLowerCase();
    return title.indexOf(q) > -1 || title.lastIndexOf(q) > -1;
  });
}

function order(data, column, direction) {
  return data.sort((first, second) => {
    let orderValue = 0;
    if (first[column] < second[column]) {
      orderValue = -1;
    }
    if (first[column] > second[column]) {
      orderValue = 1;
    }

    return direction === "ASC" ? orderValue : orderValue * -1;
  });
}

module.exports = {
  paginate,
  search,
  order,
};
