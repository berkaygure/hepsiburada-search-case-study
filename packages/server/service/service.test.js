const { paginate, order, search } = require("./service");

const data = [
  {
    id: 1,
    title: "Ayca",
    price: 100,
  },
  {
    id: 2,
    title: "Berkay",
    price: 50,
  },

  {
    id: 3,
    title: "Cagatay",
    price: 90,
  },

  {
    id: 4,
    title: "Zeynep",
    price: 10,
  },
];

describe("TEST services", () => {
  describe("TEST pagination", () => {
    it("should paginate correctly", () => {
      const paged = paginate(data, 1, 2);
      expect(paged).toHaveLength(2);
    });
    it("should get second page correctly", () => {
      const paged = paginate(data, 2, 2);
      expect(paged).toStrictEqual([
        {
          id: 3,
          title: "Cagatay",
          price: 90,
        },

        {
          id: 4,
          title: "Zeynep",
          price: 10,
        },
      ]);
    });
    it("should paginate default total", () => {
      const paged = paginate(data, 1);
      expect(paged).toHaveLength(data.length);
    });
  });

  describe("TEST order", () => {
    it("should order asc", () => {
      const ordered = order(data, "price", "ASC");
      expect(ordered).toStrictEqual([
        {
          id: 4,
          title: "Zeynep",
          price: 10,
        },
        {
          id: 2,
          title: "Berkay",
          price: 50,
        },
        {
          id: 3,
          title: "Cagatay",
          price: 90,
        },

        {
          id: 1,
          title: "Ayca",
          price: 100,
        },
      ]);
    });

    it("should order DESC", () => {
      const ordered = order(data, "price", "DESC");
      expect(ordered).toStrictEqual([
        {
          id: 1,
          title: "Ayca",
          price: 100,
        },

        {
          id: 3,
          title: "Cagatay",
          price: 90,
        },

        {
          id: 2,
          title: "Berkay",
          price: 50,
        },
        {
          id: 4,
          title: "Zeynep",
          price: 10,
        },
      ]);
    });

    it("should order alphabetically ASC", () => {
      const ordered = order(data, "title", "ASC");
      expect(ordered).toStrictEqual([
        {
          id: 1,
          title: "Ayca",
          price: 100,
        },
        {
          id: 2,
          title: "Berkay",
          price: 50,
        },
        {
          id: 3,
          title: "Cagatay",
          price: 90,
        },
        {
          id: 4,
          title: "Zeynep",
          price: 10,
        },
      ]);
    });

    it("should order alphabetically DESC", () => {
      const ordered = order(data, "title", "DESC");
      expect(ordered).toStrictEqual([
        {
          id: 4,
          title: "Zeynep",
          price: 10,
        },

        {
          id: 3,
          title: "Cagatay",
          price: 90,
        },
        {
          id: 2,
          title: "Berkay",
          price: 50,
        },
        {
          id: 1,
          title: "Ayca",
          price: 100,
        },
      ]);
    });
  });

  describe("TEST search", () => {
    it("should search by title", () => {
      const searched = search(data, "berkay");
      expect(searched).toStrictEqual([
        {
          id: 2,
          title: "Berkay",
          price: 50,
        },
      ]);
    });

    it("should search by title case-insensitive", () => {
      const searched = search(data, "BERKAY");
      expect(searched).toStrictEqual([
        {
          id: 2,
          title: "Berkay",
          price: 50,
        },
      ]);
    });

    it("should NOT search when query empty", () => {
      const searched = search(data, "");
      expect(searched).toHaveLength(data.length);
    });
  });
});
